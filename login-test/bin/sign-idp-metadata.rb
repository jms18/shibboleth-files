#!/usr/bin/env ruby

require 'thor';
require 'date';
require 'fileutils'
require 'rexml/document';
include REXML;

#class SignMetadata < Thor::Group
class SignMetadata < Thor
  
  desc "sign idp-metadata.xml", "Signs a SAML metadata document with an <EntityDescriptor> root element.";
  long_desc \
<<-LONGDESC

`sign-idp-metadata --in filename --out filename --prefix_id CASE --xmlsectool /usr/local/xmlsectool --key private_key --cert public_cert --days n`

By default, it will use SHA-256 digest algorithm; any existing "ID" and "validUntil" attribute on the root element <EntityDescriptor> will be replaced with datetime stamps one week in the future of the execution time. It will remove existing <Signature> nodes that are direct children of the parent <EntityDescriptor> replacing them with the provided signing information.

DEFAULTS:
  STDIN
  STDOUT
  /usr/local/xmlsectool/xmlsectool.sh
  prefix id "CASE"
  days 7
  
LONGDESC

  option :in, :required => true;
  option :out, :required => true;
  option :prefix_id, :default => 'CASE';
  option :xmlsectool, :required => true;
  option :key, :required => true;
  option :cert, :required => true;
  option :days, :type => :numeric, :default => 7;
  def sign
    # cert, sig_alg, signature, query_string = [:cert, :sig_alg, :signature, :query_string].map { |k| params[k]}
    days = options[:days] ? options[:days] : 7;
    metadata_file_in = options[:in];
    metadata_file_out = options[:out];
    prefix_id = options[:prefix_id] ? options[:prefix_id] : 'CASE';
    key = options[:key];
    cert = options[:cert];
    
    xml_id = prefix_id + DateTime.now.strftime("%Y%m%d");
    doc = Document.new(File.new(metadata_file_in));
    doc.root.add_attribute("ID", xml_id);
    doc.root.add_attribute("validUntil", (DateTime.now + days).strftime("%FT%TZ"));
    
    XPath.each(doc, "/a:EntityDescriptor/b:Signature", 
      {"a" => "urn:oasis:names:tc:SAML:2.0:metadata", 
        "b" => "http://www.w3.org/2000/09/xmldsig#"}) { |element| doc.root.elements.delete element }
        
    out = File.new('/tmp/' + xml_id + 'unsigned-metadata.xml', 'w');
    doc.write(out, 0);
    out.close;
    
    unless ENV['JAVA_HOME']
      java_home = `source /etc/profile.d/java.sh 2> /dev/null && echo $JAVA_HOME`.chomp
      ENV['JAVA_HOME'] = java_home unless java_home.empty? 
    end
    
    cmd_success = system(options[:xmlsectool], '--sign', 
                          '--digest', 'SHA-256', 
                          '--referenceIdAttributeName', 'ID', 
                          '--key', options[:key],
                          '--certificate', options[:cert],
                          '--inFile', '/tmp/' + xml_id + 'unsigned-metadata.xml',
                          '--outFile', '/tmp/' + xml_id + 'signed-metadata.xml');
    
    FileUtils.cp('/tmp/' + xml_id + 'signed-metadata.xml', metadata_file_out);
  end;
end;

SignMetadata.start(ARGV);