#!/bin/bash
 
IDP_HOME=/usr/local/shibboleth
 
$IDP_HOME/bin/seckeygen.sh \
    --storefile $IDP_HOME/credentials/sealer.jks \
    --storepass login-test.case.edu \
    --versionfile $IDP_HOME/credentials/sealer.kver \
    --alias secret
 
scp $IDP_HOME/credentials/sealer.* shibb3-d-2:$IDP_HOME/credentials/
#scp $IDP_HOME/credentials/sealer.* host2:$IDP_HOME/credentials/
